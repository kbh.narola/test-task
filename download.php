<?php
session_start();

$row       							= 1;
$new_array 							= array();
$unique 							= unserialize($_SESSION['unique']);

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="sample.csv"');
$data_put = $unique;

$fp_read = fopen('php://output', 'wb');
$titles = array('make', 'model', 'colour', 'capacity', 'network', 'grade', 'condition', 'count');

fputcsv($fp_read, $titles);

foreach ($data_put as $line)
{
	fputcsv($fp_read, $line);
}

fclose($fp_read);

?>