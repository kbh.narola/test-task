<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Demo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<style>
		.d-none{
			display: none;
		}
	</style>
</head>
<body>
	<div class="container">
		<form id="get_unique_records" method="post" action="/csv_count/index.php" enctype='multipart/form-data'>
			<div class="form-group mt-4">
			    <h5 for="exampleFormControlFile1">Please upload your CSV file here.</h5>
			    <input type="file" class="form-control-file" name="csv_file">
		  	</div>
		  	<div class="form-group">
			    <input type="submit" class="btn btn-primary" value="Submit" id="submitButton">
			    <div class="spinner-border d-none" role="status">
				  <span class="sr-only">Loading...</span>
				</div>
		  	</div>
		</form>
	</div>


<?php

if($_FILES)
{
	session_start();

	$new_array 				= array();
	$row  					= 0;
	if (($handle = fopen($_FILES['csv_file']['name'] , 'r')) !== FALSE)
	{
		while (($data = fgetcsv($handle, '', ',')) !== FALSE)
		{
			if($row == 0){ 
				$row++; continue; 
			}

			$final = array();
			foreach ($data as $key => $d) 
			{
				if($d != '' && $d != null)
				{
					array_push($final, $d);
				}
			}
			array_push($new_array, $final);
		}

		fclose($handle);
		
		//Using all value
		/*$serialize 					= array_map('serialize', $new_array);
		$count     					= array_count_values($serialize);
		$unique    					= array_unique($serialize);

		
		foreach ($unique as $key => &$u)
		{
			$u_count = $count[$u];
			$u       = unserialize($u);
			$u['count'] = $u_count;
		}
*/
		//On single column
		$tempArr 						= array_unique(array_column($new_array, 0));
		$unique 						= array_intersect_key($new_array, $tempArr);
		$count 							= array_count_values(array_column($new_array, 0));
		foreach ($unique as $key => &$u)
		{
		
			$u_count = $count[$u[0]];

			$u['count'] = $u_count;
		}
			
		$_SESSION["unique"] = serialize($unique);
	}

}
if(!empty($unique))
{
?>
<div class="container">
	<div class="text-right col-sm-12">
		<a class="btn btn-info mb-3" href="/csv_count/download.php">Download</a>
	</div>
	<div class="col-sm-12">
		<div class="row">
	    	<div class="col-sm-12">
				<div class="table-responsive">
					<table class="table">
						<!-- <caption>Unique Record Listing</caption> -->
						<tbody>
							<tr>
								<th>SrNo</th>
								<th>Make</th>
								<th>Model</th>
								<th>Colour</th>
								<th>Capacity</th>
								<th>Network</th>
								<th>Grade</th>
								<th>Condition</th>
								<th>Count</th>
							</tr>
						<?php
							$count = 1;
							
							foreach ($unique as $key => $row_data)
							{
							?>
							<tr>
								<td><?php echo $count; ?></td>
							<?php

									foreach ($row_data as $row)
									{
									?>
								<td><?php echo $row; ?></td>
							<?php
								}

									$count++;
								?>
							</tr>
							<?php
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
} 
?>
<script>
	$(document).on('click', '#submitButton', function(e) {
		e.preventDefault();
		$(this).hide();
		$(".spinner-border").removeClass('d-none');
		$('#get_unique_records')[0].submit();
	});
</script>
</body>
</html>
